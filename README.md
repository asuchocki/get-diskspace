### Get-DiskSpace PowerShell Function

This function will query a computer via WMI for the drive letter and capacity information for all logical disks.

#### Instructions
Add this function to your PowerShell profile.
