function Get-DiskSpace {
  param (
    [Parameter(
      Position=0,
      ValueFromPipeline=$true,
      ValueFromPipelineByPropertyName=$true
    )]
    [string[]]$ComputerName = $env:ComputerName,
    [System.Management.Automation.CredentialAttribute()]$Credential
  )

  process {

    ForEach ( $Computer in $ComputerName ) {
      try {
        if ($Credential -ne $null) {
          $disks = gwmi Win32_LogicalDisk -EA Stop -Filter "DriveType=3" -ComputerName $Computer -Credential $Credential
        } else {
          $disks = gwmi Win32_LogicalDisk -EA Stop -Filter "DriveType=3" -ComputerName $Computer
        }
        $disks | % {
          $o = New-Object –TypeName PSObject | select ComputerName,DiskName,SizeGB,FreeSpaceGB,PercentFree
          $o.ComputerName = $_.SystemName
          $o.DiskName = $_.Name
          $o.SizeGB = [math]::Round(($_.Size/1gb),2)
          $o.FreeSpaceGB = [math]::Round(($_.FreeSpace/1gb),2)
          $o.PercentFree = [math]::Round((100 * ($o.FreeSpaceGB)/($o.SizeGB)),2)

          Write $o
        }
      }
      # Access denied catch
      catch [System.UnauthorizedAccessException]{
        Write "${Computer}: Access Denied"
      }
      # Generic catch
      catch [System.Exception]{
        Write "${Computer}: Unable to collect WMI"
      }
    }
  }
}
